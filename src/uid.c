/*
 * Copyright (C) 2018 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <pthread.h>
#include <sys/random.h>

#include "uid.h"
#include "base64.h"

/*
 * uid are object having a text associated that is unic within an
 * uidset.
 */

/*
 * MUST be multiple of 4.
 * 4 ensures a minimum of 2^24~16*10^6 differents places cause 24=8*(3/4)*4
 */
#define MINLEN   4

/*
 * MUST be multiple of 4.
 * 4 ensures 2^24 times more uids in the set
 */
#define INCLEN   4

/*
 * With MINLEN = 4, the minimal count of items is 2^24 ~ 16.10^6.
 *
 * We target a maximum uid count of 10^6.
 *
 * It means that the probability p to pick randomly an already used
 * uid will be better than 1/16 ~ 10^6 / 2^24
 *
 * So in the worst case for this conditions after N trials, the
 * probability to pick only used uids is p^N
 *
 * For having p^N less than 10^-6 we can use (1/16)^N < 2^-20
 * to compute N. It leads to N >= 5
 */
#define DEFTRIAL 5

struct uidset;

/**
 * Structure for a unic id
 */
struct uid
{
	/** link to a next */
	struct uid *next;

	/** container */
	struct uidset *set;

	/** some user data */
	void *data;

	/** refence count */
	unsigned refcount;

	/** text length (without tailing zero), MUST multiple of 4 */
	int txtlen;

	/** identifier */
	char text[1];
};

/**
 * structure that records a set of unic ids.
 * The ids are unic only within a set.
 */
struct uidset
{
	/** head of items */
	struct uid *head;

	/** mutual exclusion on the list */
	pthread_mutex_t mutex;

	/** refence count */
	unsigned refcount;

	/** count of items */
	unsigned count;

	/** length of the uid key */
	int minlen;
};

/**
 * Compute length: the lowest multiple of 4 greater or equal to max(minlen, MINLEN)
 * @param minlen the minimal length of the uid to create
 * @return the length of the uid to create
 */
static int complen(int minlen)
{
	return minlen <= MINLEN ? MINLEN : minlen + (3 & (-minlen));
}

/**
 * Generate an uid text (zero terminated) of length 'txtlen' in 'buffer'
 *
 * @param buffer the buffer where to store the generate uid text
 * @param txtlen the length of the text to generate without the terminating null
 *               must be a multiple of 4
 */
static void genid(
	char *buffer,
	int txtlen
) {
	ssize_t rgr __attribute__ ((unused));
	int ftl; /* ftl: forth of textlen */
	int x; /* state of encoding */
	int blen; /* length of binary random data */
	char *bbuf; /* head of binary random data */

	/*
	 * The generation of a uid is based on base64 encoding of some
	 * random value.
	 * Base64 encoding of 3.N bytes is 4.N bytes length.
	 * This explains that the text length L must be a multiple of 4.
	 * Then having L = 4.N the computation tells N = L/4 and 3.N = L-N.
	 * As an optimisation of memory usage, the same 'buffer' is used
	 * to handle the random value and the generated uid text. The trick
	 * is that for avoiding memory crush, the random value is generated
	 * at the end of the buffer.
	 * +-------+-------+-------+-------+0
	 * ^        ^
	 * `uid     `bbuf
	 */
	ftl = txtlen >> 2;
	blen = txtlen - ftl;
	bbuf = &buffer[1 + ftl];
	rgr = getrandom(bbuf, blen, 0);
	x = 0;
	base64_encode_put(bbuf, blen, &buffer, &x, Base64_Variant_URL_Trunc);
	base64_encode_flush(&buffer, &x, Base64_Variant_URL_Trunc);
	*buffer = 0;
}

/**
 * Search within 'uidset' the uid matching the given 'text'
 * @param uidset the set to search
 * @param text the uid text to search
 * @return the uid structure found or NULL if none matched
 */
static struct uid *search(
	struct uidset *uidset,
	const char *text
) {
	struct uid *uid = uidset->head;
	while(uid && strcmp(uid->text, text))
		uid = uid->next;
	return uid;
}

/**
 * Creates a new text value for the uid
 * @param uidset the set within wich the uid must be unic
 * @param uid the uid to set
 * @return 0 in case of success or -1 with errno = EGAIN when none generated
 */
static int newid(
	struct uidset *uidset,
	struct uid *uid
) {
	char *temp;
	int trialcount;

	/* default trial count */
	trialcount = DEFTRIAL;

	/* use local buffer to allow renew of uid (see uid_change) */
	temp = alloca(uid->txtlen + 1);
	do {
		genid(temp, uid->txtlen);
		if (!search(uidset, temp)) {
			strcpy(uid->text, temp);
			return 0;
		}
	} while(--trialcount);

	/* max trial reached */
	errno = EAGAIN;
	return -1;
}

/**
 * Creates a uidset in 'result' for uid of default 'minlen'
 * @param result where to put the handler to the created uidset
 * @param minlen the default minlen of created uids
 * @return 0 in case of success or -1 with errno = ENOMEM
 */
int uidset_create(
	struct uidset **result,
	int minlen
) {
	struct uidset *uidset;

	uidset = *result = malloc(sizeof *uidset);
	if (!uidset)
		return -1;

	uidset->head = NULL;
	uidset->count = 0;
	pthread_mutex_init(&uidset->mutex, NULL);
	uidset->minlen = minlen;
	uidset->refcount = 1;

	return 0;
}

/**
 * Increment the use count of the 'uidset'
 * @param uidset the uidset whose use count is to be incremented
 * @return the uidset
 */
struct uidset *uidset_addref(
	struct uidset *uidset
) {
	if (uidset)
		__atomic_add_fetch(&uidset->refcount, 1, __ATOMIC_RELAXED);
	return uidset;
}

/**
 * Decrement the use count of the 'uidset' and possibly destroy it if no
 * more used.
 * @param uidset the uidset whose use count is to be decremented
 */
void uidset_unref(
	struct uidset *uidset
) {
	if (uidset && !__atomic_sub_fetch(&uidset->refcount, 1, __ATOMIC_RELAXED)) {
		pthread_mutex_destroy(&uidset->mutex);
		free(uidset);
	}
}

/**
 * Search the uid of 'text' within the 'uidset'
 * @param uidset the uidset to inspect
 * @param text the text of the uid to search
 * @return the found uid or NULL if not found
 */
struct uid *uidset_search(
	struct uidset *uidset,
	const char *text
) {
	struct uid *uid;

	pthread_mutex_lock(&uidset->mutex);
	uid = search(uidset, text);
	pthread_mutex_unlock(&uidset->mutex);
	if (!uid)
		errno = ENOENT;
	return uid;
}

/**
 * Get the count of uid of the 'uidset'
 * @param uidset the set whose item count is queried
 * @return the count of uid of the set
 */
unsigned uidset_count(struct uidset *uidset)
{
	return uidset->count;
}

/**
 * Call 'callback' for all uid of 'uidset'
 * It is safe to unref the recived 'uid' within the callback
 * @param uidset the uidset to process
 * @param callback the callback to call receives: (closure, uid, data)
 * @param closure the closure to pass to the callback
 */
void uidset_for_all(
	struct uidset *uidset,
	void (*callback)(void *closure, struct uid *uid, void *data),
	void *closure
) {
	unsigned n, i;
	struct uid *uid, **arruid;

	pthread_mutex_lock(&uidset->mutex);
	n = uidset->count;
	if (n == 0)
		/* nothing to do */
		pthread_mutex_unlock(&uidset->mutex);
	else {
		/* copy uids (with addref) in local array */
		arruid = alloca(n * sizeof *arruid);
		uid = uidset->head;
		i = 0;
		while (uid) {
			arruid[i++] = uid_addref(uid);
			uid = uid->next;
		}
		assert(i == n);
		pthread_mutex_unlock(&uidset->mutex);

		/* process the saved uids */
		while(i) {
			uid = arruid[--i];
			callback(closure, uid, uid->data);
			uid_unref(uid);
		}
	}
}

/**
 * Creates a new random base64url uid of 'minlen' and 'data' in the 'uidset'
 * @param result where to put created uid
 * @param uidset the set of uid
 * @param data the data to set to the uid
 * @param minlen the minimal length of the random uid
 * @return 0 in case of success or -1 with errno = ENOMEM
 */
int uid_create_length(
	struct uid **result,
	struct uidset *uidset,
	void *data,
	int minlen
) {
	struct uid *uid;
	int len;
	int rc;

	len = complen(minlen);
	for (;;) {
		*result = uid = malloc(len + sizeof *uid);
		if (!uid)
			return -1;

		uid->set = uidset_addref(uidset);
		uid->refcount = 1;
		uid->data = data;
		uid->txtlen = len;

		/* */
		pthread_mutex_lock(&uidset->mutex);
		rc = newid(uidset, uid);
		if (rc == 0) {
			/* done so add it */
			uid->next = uidset->head;
			uidset->head = uid;
			uidset->count++;
			pthread_mutex_unlock(&uidset->mutex);
			return 0;
		}

		pthread_mutex_unlock(&uidset->mutex);
		free(uid);
		len = complen(len + INCLEN);
	}
}

/**
 * Creates a new random base64url uid of the default minlen and 'data' in the 'uidset'
 * @param result where to put created uid
 * @param uidset the set of uid
 * @param data the data to set to the uid
 * @return 0 in case of success or -1 with errno = ENOMEM
 */
int uid_create(
	struct uid **result,
	struct uidset *uidset,
	void *data
) {
	return uid_create_length(result, uidset, data, uidset->minlen);
}

/**
 * Creates a new uid of the given 'text' and 'data' in the 'uidset'
 * @param result where to put created uid
 * @param uidset the set of uid
 * @param text the text to set for the uid
 * @param data the data to set to the uid
 * @return 0 in case of success or -1 with errno = ENOMEM or EEXIST
 */
int uid_create_for_text(
	struct uid **result,
	struct uidset *uidset,
	const char *text,
	void *data
) {
	struct uid *uid;
	int len, rc = -1;

	if (!text || !*text)
		return uid_create(result, uidset, data);

	pthread_mutex_lock(&uidset->mutex);
	uid = search(uidset, text);
	if (uid) {
		uid = NULL;
		errno = EEXIST;
	} else {
		len = complen((int)strlen(text));
		uid = malloc(len + sizeof *uid);
		if (uid) {
			uid->set = uidset_addref(uidset);
			uid->refcount = 1;
			uid->data = data;
			uid->txtlen = len;
			strcpy(uid->text, text);
			uid->next = uidset->head;
			uidset->head = uid;
			uidset->count++;
			rc = 0;
		}
	}
	pthread_mutex_unlock(&uidset->mutex);
	*result = uid;
	return rc;
}

/**
 * Increment the use count of 'uid'
 * @param uid the uid whose use count is to increment
 * @return the uid
 */
struct uid *uid_addref(
	struct uid *uid
) {
	if (uid)
		__atomic_add_fetch(&uid->refcount, 1, __ATOMIC_RELAXED);
	return uid;
}

/**
 * Decrement the use count of 'uid' and possibly destroy it
 * @param uid the uid whose use count is to decrement
 */
void uid_unref(
	struct uid *uid
) {
	struct uidset *set;
	struct uid **puid;

	if (uid && !__atomic_sub_fetch(&uid->refcount, 1, __ATOMIC_RELAXED)) {
		set = uid->set;
		pthread_mutex_lock(&set->mutex);
		puid = &set->head;
		while(*puid) {
			if (*puid != uid)
				puid = &(*puid)->next;
			else {
				*puid = uid->next;
				set->count--;
				break;
			}
		}
		pthread_mutex_unlock(&set->mutex);
		free(uid);
		uidset_unref(set);
	}
}

/**
 * Change the text of the 'uid'
 * @param uid uid whose text is to change
 * @return  in case of success or -1 in case of error
 */
int uid_change(
	struct uid *uid
) {
	int rc;

	struct uidset *uidset = uid->set;
	pthread_mutex_lock(&uidset->mutex);
	rc = newid(uidset, uid);
	pthread_mutex_unlock(&uidset->mutex);
	return rc;
}

/**
 * Get the text of the 'uid'
 * @param uid the uid whose text is queried
 * @return the text of the uid
 */
const char *uid_text(
	struct uid *uid
) {
	return uid->text;
}

/**
 * Get the uidset of the 'uid'
 * @param uid the uid whose uidset is queried
 * @return the uidset of the uid
 */
struct uidset *uid_set(
	struct uid *uid
) {
	return uid->set;
}

/**
 * Get the data of the 'uid'
 * @param uid the uid whose data is queried
 * @return the data of the uid
 */
void *uid_data(
	struct uid *uid
) {
	return uid->data;
}

/**
 * Set the data of the 'uid'
 * @param uid the uid whose data is to be set
 * @param data the data to be set
 */
void uid_data_set(
	struct uid *uid,
	void *data
) {
	uid->data = data;
}

