/*
 * Copyright (C) 2018 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <time.h>

struct cred;
struct credset;

extern int credset_create(struct credset **result);
extern int credset_load(struct credset **result, const char *file);
extern int credset_store(struct credset *credset, const char *file);

extern struct credset *credset_addref(struct credset *credset);
extern void credset_unref(struct credset *credset);

extern void credset_purge(struct credset *credset, time_t time);
extern void credset_for_all(
	struct credset *credset,
	void (*callback)(void *closure, struct cred *cred),
	void *closure
);

extern int credset_new_cred(struct credset *credset, struct cred **result);
extern int credset_search_token(struct credset *credset, const char *token, struct cred **result, time_t *expire);
extern int credset_search_bearer(struct credset *credset, const char *bearer, struct cred **result, time_t *expire);
extern int credset_search(struct credset *credset, const char *code, struct cred **result, time_t *expire);

extern struct cred *cred_addref(struct cred *cred);
extern void cred_unref(struct cred *cred);

extern void cred_revoke(struct cred *cred);

extern void cred_set_expire(struct cred *cred, time_t expire);
extern int cred_is_expired(struct cred *cred, time_t time);
extern time_t cred_get_expire(struct cred *cred);

extern int cred_add_scope(struct cred *cred, const char *scope);
extern int cred_sub_scope(struct cred *cred, const char *scope);
extern int cred_has_scope(struct cred *cred, const char *scope);

extern int cred_get_token(struct cred *cred, char **result);
extern int cred_get_bearer(struct cred *cred, time_t expire, char **result);

extern void *cred_data(struct cred *cred);
extern void cred_set_data(struct cred *cred, void *data);
