/*
 * Copyright (C) 2018 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>

#include <json-c/json.h>

#include "curl-wrap.h"

#if !defined(DEFAULTNAME)
#    define DEFAULTNAME	"CYNAGOAUTH_TOKEN"
#endif
#if !defined(DEFAULTNAMETYPE)
#    define DEFAULTNAMETYPE	DEFAULTNAME"_TYPE"
#endif
#if !defined(DEFAULTNAMEEXPIRE)
#    define DEFAULTNAMEEXPIRE	DEFAULTNAME"_EXPIRE"
#endif
#if !defined(VARURL)
#    define VARURL	"CYNAGOAUTH_URL"
#endif
#if !defined(DEFAULTURL)
#    define DEFAULTURL	"http://localhost:7777/tok"
#endif

const char shortopts[] = "+hn:r:t:u:";

const struct option longopts[] = {
	{ "help",    0, 0, 'h' },
	{ "name",    1, 0, 'n' },
	{ "replace", 1, 0, 'r' },
	{ "token",   1, 0, 't' },
	{ "url",     1, 0, 'u' },
	{ 0, 0, 0, 0 }
};

const char helpmsg[] =
	"\n"
	"usage: %s [options...] program [args...]\n"
	"\n"
	"Ask an OAuth2 server for an access token and launches the given program\n"
	"with this retrieved token. The URL of the token end point to be queried\n"
	"can be set by option (see below) or environment variable "VARURL".\n"
	"The default value is "DEFAULTURL"\n"
	"\n"
	"When launched the program has the following environment variables defined:\n"
	"\n"
	"  - the access token   "DEFAULTNAME"\n"
	"\n"
	"The arguments of the program to launch are scanned and patterns for the token\n"
	"are substituted by the effective value of the token. The default pattern is @t\n"
	"\n"
	"Options:\n"
	"\n"
	" -h, --help             this help\n"
	" -n, --name NAME        name of the environement variable to set\n"
	" -r, --replace PATTERN  redefine the pattern to be replaced\n"
	" -t, --token TOKEN      the token to use, token end point is not queried\n"
	" -u, --url URL          URL of the token end point\n"
	"\n"
;

void printhelp(char *prog)
{
	prog = strchr(prog, '/') ? strrchr(prog, '/') + 1 : prog;
	printf(helpmsg, prog);
	exit(0);
}

char *optname = NULL;
char *optreplace = NULL;
char *opttoken = NULL;
char *opturl = NULL;

int runac;
char **runav;

char *rewrite_search(char *string, const char *defs[], int *idxpat)
{
	char *sfound, *sit;
	const char *spat;
	int ifound, iit;

	ifound = -1;
	sfound = NULL;
	for (iit = 0 ; (spat = defs[iit]) ; iit += 2) {
		if (*spat) {
			sit = strstr(string, spat);
			if (sit && (sfound == NULL || sfound > sit ||
			 (sfound == sit && strlen(spat) > strlen(defs[ifound])))) {
				ifound = iit;
				sfound = sit;
			}
		}
	}
	*idxpat = ifound;
	return sfound;
}


char *rewrite_replace(char *string, const char *defs[])
{
	char *resu, *it, *wr;
	const char *spat, *srep;
	size_t add, sub, lpat, lrep;
	int idx;

	/* get the changes */
	add = sub = 0;
	it = rewrite_search(string, defs, &idx);
	while (it) {
		spat = defs[idx];
		srep = defs[idx + 1];
		lpat = strlen(spat);
		lrep = strlen(srep);
		sub += lpat;
		add += lrep;
		it = rewrite_search(&it[lpat], defs, &idx);
	}

	/* return arg when no change */
	if (!sub)
		return string;

	/* allocates the result */
	resu = malloc(strlen(string) + add - sub + 1);
	if (!resu) {
		fprintf(stderr, "out of memory");
		exit(1);
	}

	/* instanciate the arguments */
	wr = resu;
	for (;;) {
		it = rewrite_search(string, defs, &idx);
		if (it == NULL) {
			strcpy(wr, string);
			return resu;
		}
		if (it != string)
			wr = mempcpy(wr, string, it - string);
		spat = defs[idx];
		srep = defs[idx + 1];
		lpat = strlen(spat);
		lrep = strlen(srep);
		wr = mempcpy(wr, srep, lrep);
		string = &it[lpat];
	}
}

char *rewrite(char *arg, const char *token)
{
	const char *defs[5];
	
	defs[0] = optreplace ?: "@t";
	defs[1] = token;
	defs[2] = NULL;
	return rewrite_replace(arg, defs);
}

void onreply(void *closure, int status, CURL *curl, const char *result, size_t size)
{
	struct json_object **object = closure;

	if (size == 0) {
		fprintf(stderr, "HTTP error %s\n", result?:"?");
		exit(1);
	}

	*object = json_tokener_parse(result);
}

struct json_object *querytoken(const char *url)
{
	static const char *req[] = { "grant_type", "client_credentials", NULL };

	struct json_object *object;
	CURL *curl;
	
	curl = curl_wrap_prepare_post(url, NULL, req);
	if (curl == NULL) {
		fprintf(stderr, "out of memory\n");
		exit(1);
	}
	curl_wrap_do(curl, onreply, &object);
	return object;
}

void process_arguments(int ac, char **av)
{
	int optid;

	if (ac < 2) {
		fprintf(stderr, "wrong count of arguments\n");
		exit(1);
	}
	for(;;) {
		optid = getopt_long(ac, av, shortopts, longopts, NULL);
		if (optid < 0) {
			/* end of options */
			runac = ac - optind;
			runav = &av[optind];
			return;
		}
		switch (optid) {
		case 'h':
			printhelp(av[0]);
			break;
		case 'n':
			optname = optarg;
			break;
		case 'r':
			optreplace = optarg;
			break;
		case 't':
			opttoken = optarg;
			break;
		case 'u':
			opturl = optarg;
			break;
		default:
			fprintf(stderr, "Bad option detected");
			exit(1);
		}
	}
}

int main(int ac, char **av)
{
	struct json_object *object, *type, *tok;
	const char *token;
	int rc, i;

	/* process arguments */
	process_arguments(ac, av);

	if (opttoken)
		token = opttoken;
	else {
		if (!opturl) {
			opturl = getenv(VARURL);
			if (!opturl)
				opturl = DEFAULTURL;
		}
		object = querytoken(opturl);
		if (!json_object_object_get_ex(object, "access_token", &tok)) {
			fprintf(stderr, "no token returned\n");
			exit(1);
		}
		if (!json_object_object_get_ex(object, "token_type", &type)) {
			fprintf(stderr, "no token type\n");
			exit(1);
		}
		if (!json_object_is_type(type, json_type_string)
			|| !json_object_is_type(tok, json_type_string)
			|| strcmp(json_object_get_string(type), "bearer")) {
			fprintf(stderr, "unknown token type\n");
			exit(1);
		}

		token = json_object_get_string(tok);
	}

	/* set the environment variable */
	if (!optname)
		optname = DEFAULTNAME;
	if (*optname) {
		rc = setenv(optname, token, 1);
		if (rc < 0) {
			fprintf(stderr, "out of memory\n");
			exit(1);
		}
	}

	for (i = 1 ; i < runac ; i++)
		runav[i] = rewrite(runav[i], token);

	execv(runav[0], runav);
	fprintf(stderr, "can't execute %s: %s\n", runav[0], strerror(errno));
	exit(1);
	return 0;
}

