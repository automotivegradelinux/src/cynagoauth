/*
 * Copyright (C) 2017 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "base64.h"

static const char variant_chars[4] = {
	'+', '/',	/* normal variant */
	'-', '_'	/* url variant */
};

/** encode 6bits value to its base64 character, following the variant */
static char eb64(int x, const char *variant)
{
	if (x < 52)
		return (char)(x + (x < 26 ? 'A' : ('a' - 26)));
	else
		return x < 62 ? (char)(x - (52 - '0')) : variant[x - 62];
}

/** decode base64 character to its 6bits value, following the variant */
static int db64(char c)
{
	if (c >= 'A' && c <= 'Z')
		return (int)(c - 'A');
	if (c >= 'a' && c <= 'z')
		return 26 + (int)(c - 'a');
	if (c >= '0' && c <= '9')
		return 52 + (int)(c - '0');
	if (c == '-' || c == '+')
		return 62;
	if (c == '_' || c == '/')
		return 63;
	return -1;
}

/** return the encoded length for a given buffer length and a variant */
size_t base64_decode_length(size_t length)
{
	size_t d = length >> 2;
	size_t r = length & 3;
	return d * 3 + (r <= 1 ? 0 : r - 1);
}

/** return the encoded length for a given buffer length and a variant */
void base64_decode_put(const char *buffer, size_t size, char **pout, int *psta)
{
	const char *i, *e;
	char *out, c;
	int state, x;

	/* encode */
	out = *pout;
	state = *psta & 255;
	i = buffer;
	e = &i[size];
	while (i < e) {
		c = *i++;
		if (c == '=')
			continue;
		x = db64(c);
		if (x < 0)
			continue;
		switch(state & 3) {
		case 0:
			state = (x << 2) | 1;
			break;
		case 1:
			*out++ = (char)((state & 0374) | ((x >> 4) & 0003));
			state = (x << 4) | 2;
			break;
		case 2:
			*out++ = (char)((state & 0360) | ((x >> 2) & 0017));
			state = (x << 6) | 3;
			break;
		case 3:
			*out++ = (char)((state & 0300) | (x & 0077));
			state = 0;
			break;
		}
	}
	*pout = out;
	*psta = state;
}

/** return the encoded length for a given buffer length and a variant */
size_t base64_encode_length(size_t length, enum base64_variant variant)
{
	size_t d = length / 3;
	size_t r = length % 3;
	return (d << 2) + (r == 0 ? 0
				  : (variant & Base64_Variant_Trunc) ? 1 + r
								     : 4);
}

void base64_encode_put(const char *buffer, size_t size, char **pout, int *psta,
						enum base64_variant variant)
{
	const char *v;
	const unsigned char *i, *e;
	char *out;
	int state, c;

	/* assumes that Base64_Variant_URL==2 */
	v = &variant_chars[variant & Base64_Variant_URL];

	/* encode */
	out = *pout;
	state = *psta & 255;
	i = (const unsigned char*)buffer;
	e = &i[size];
	while (i < e) {
		if (!(state & 1)) {
			/* encode groups of 3 bytes to 4 chars */
			while (e - i >= 3) {
				c = (int)*i++;
				*out++ = eb64(c >> 2, v);
				c &= 3;
				c <<= 8;
				c |= (int)*i++;
				*out++ = eb64(c >> 4, v);
				c &= 15;
				c <<= 8;
				c |= (int)*i++;
				*out++ = eb64(c >> 6, v);
				*out++ = eb64(c & 63, v);
			}
			/* encode remaining if needed */
			if (i < e) {
				c = (int)*i++;
				*out++ = eb64(c >> 2, v);
				state = ((c & 3) << 6) | 1;
			}
		} else if (!(state & 2)) {
			c = (int)*i++;
			*out++ = eb64((state >> 2) | ((c >> 4) & 15), v);
			state = ((c & 15) << 4) | 3;
		} else {
			c = (int)*i++;
			*out++ = eb64((state >> 2) | ((c >> 6) & 3), v);
			*out++ = eb64(c & 63, v);
			state = 0;
		}
	}
	*pout = out;
	*psta = state;
}

void base64_encode_flush(char **pout, int *psta, enum base64_variant variant)
{
	const char *v;
	char *out;
	int state;

	/* assumes that Base64_Variant_URL==2 */
	v = &variant_chars[variant & Base64_Variant_URL];

	/* encode */
	out = *pout;
	state = *psta & 255;
	switch (*psta & 3) {
	case 0:
		break;
	case 1:
		*out++ = eb64(state >> 2, v);
		if (!(variant & Base64_Variant_Trunc)) {
			*out++ = '=';
			*out++ = '=';
		}
		break;
	default:
		*out++ = eb64(state >> 2, v);
		if (!(variant & Base64_Variant_Trunc))
			*out++ = '=';
		break;
	}
	*pout = out;
	*psta = 0;
}

char *base64_encode_array_variant(const char * const *args, size_t count,
						enum base64_variant variant)
{
	char *buffer, *iter;
	size_t n, sz, *asz;
	int state;

	/* compute size and allocate */
	asz = alloca(count * sizeof *asz);
	for (sz = n = 0 ; n < count ; n++)
		sz += asz[n] = strlen(args[n]);
	sz = base64_encode_length(sz, variant);
	buffer = malloc(1 + sz);
	if (!buffer)
		return NULL;

	/* encode */
	iter = buffer;
	state = 0;
	for (n = 0 ; n < count ; n++)
		base64_encode_put(args[n], asz[n], &iter, &state, variant);
	base64_encode_flush(&iter, &state, variant);
	*iter = 0;
	return buffer;
}

char *base64_encode_multi_variant(const char * const *args,
						enum base64_variant variant)
{
	size_t count;

	for (count = 0 ; args[count] ; count++);
	return base64_encode_array_variant(args, count, variant);
}

char *base64_encode_variant_variant(const char *arg,
						enum base64_variant variant)
{
	return base64_encode_array_variant(&arg, !!arg, variant);
}

char *base64_encode_buffer_variant(const void *buffer, size_t size,
						enum base64_variant variant)
{
	int state;
	char *outbuf, *iter;
	size_t sz;

	sz = base64_encode_length(size, variant);
	outbuf = malloc(1 + sz);
	if (outbuf) {
		iter = outbuf;
		state = 0;
		base64_encode_put(buffer, size, &iter, &state, variant);
		base64_encode_flush(&iter, &state, variant);
		*iter = 0;
	}
	return outbuf;
}

char base64_encode_value_variant(int value, enum base64_variant variant)
{
	return eb64(value & 63, &variant_chars[variant & Base64_Variant_URL]);
}

/* vim: set colorcolumn=80: */
