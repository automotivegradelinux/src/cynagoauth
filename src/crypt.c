/*
 * Copyright (C) 2019 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#include <stdio.h>
#include <string.h>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/random.h>

#include <openssl/blowfish.h>

#include "crypt.h"

#define KEYLEN     56

struct crypt {
	BF_KEY key;
	uint16_t blocksize;
};

/******************************************************************************/

static inline uint8_t *scramble_array(struct crypt *crypt)
{
	return (uint8_t*)&((&crypt->blocksize)[1]);
}

static inline uint16_t *shuffle_array(struct crypt *crypt)
{
	return (uint16_t*)&(scramble_array(crypt)[crypt->blocksize]);
}

static inline size_t compute_size(size_t blocksize)
{
	return sizeof(struct crypt) + blocksize * sizeof(uint16_t) + blocksize;
}

/******************************************************************************/

static inline void getrand(void *buffer, size_t length)
{
	ssize_t rgr __attribute__ ((unused));
	rgr = getrandom(buffer, length, 0);
}

static void xor(uint8_t *data, const uint8_t *ref, uint16_t n)
{
	uint16_t i;
	for(i = 0 ; i < n ; i++)
		data[i] ^= ref[i];
}

static void scramble(uint8_t *data, struct crypt *crypt)
{
	xor(data, scramble_array(crypt), crypt->blocksize);
}

static void shuffle(uint8_t *data, struct crypt *crypt, int enc)
{
	uint16_t *s = shuffle_array(crypt);
	int i, d, k, a, n = crypt->blocksize;
	uint8_t p, t;

	d = n;
	a = enc ? 1 : n - 1;
	i = 0;
	k = s[i];
	p = data[k];
	do {
		i += a;
		if (i >= n)
			i -= n;
		k = s[i];
		t = data[k];
		data[k] = p;
		p = t;
	} while (--d) ;
}

static void docrypt(uint8_t *data, struct crypt *crypt, int enc)
{
	int i, n = crypt->blocksize;
	enc = enc ? BF_ENCRYPT : BF_DECRYPT;
	for(i = 0 ; i < n ; i += 8)
		BF_ecb_encrypt(&data[i], &data[i], &crypt->key, enc);
}

#ifdef GCD_SHUFFLE
static int gcd(int x, int y)
{
	int z;
	for (;;) {
		z = y % x;
		if (!z)
			return x;
		y = x;
		x = z;
	}
	return x;
}
#endif

static void initcrypt(struct crypt *crypt)
{
	unsigned char key[KEYLEN];
	uint16_t *sh = shuffle_array(crypt);
	uint8_t *sc = scramble_array(crypt);
	uint16_t k;
	int i, j, n = crypt->blocksize;

	/* create the BF key */
	getrand(key, sizeof key);
	BF_set_key(&crypt->key, sizeof key, key);

	/* create the shuffling */
#ifdef GCD_SHUFFLE
	j = (n >> 1) - 3;
	while (j > 1 && gcd(j, n) > 1)
		j--;
	k = 0;
	for (i = 0 ; i < n ; i++) {
		k = (uint16_t)((k + j) % n);
		sh[i] = k;
	}
#else
	for (i = 0 ; i < n ; i++)
		sh[i] = (uint16_t)i;
	getrand(sc, n - 1);
	for (i = 0 ; i < n - 1 ; i++) {
		j = i + (sc[i] % (n - i));
		k = sh[i];
		sh[i] = sh[j];
		sh[j] = k;
	}
#endif

	/* create the scrambling */
	getrand(sc, n);
}

/******************************************************************************/

int crypt_create(struct crypt **result, size_t blocksize)
{
	struct crypt *crypt;

	/* clear the result */
	*result = NULL;

	/* not a too big blocksize */
	if (blocksize > UINT16_MAX)
		return -EINVAL;

	/* blocksize must be multiple of 8 */
	if ((blocksize & 7) != 0)
		return -EINVAL;

	/* not a too small blocksize */
	if (blocksize == 0)
		return -EINVAL;

	/* allocate */
	crypt = malloc(compute_size(blocksize));
	if (!crypt)
		return -ENOMEM;

	/* init */
	crypt->blocksize = (uint16_t)blocksize;
	initcrypt(crypt);
	*result = crypt;
	return 0;
}

int crypt_load(struct crypt **result, int fd)
{
	struct crypt *crypt;
	size_t size;
	ssize_t rc;

	/* clear the result */
	*result = NULL;

	/* read the size */
	rc = read(fd, &size, sizeof size);
	if (rc != sizeof size)
		return -errno;

	/* allocate the data */
	crypt = malloc(compute_size(size));
	if (!crypt)
		return -ENOMEM;

	/* read the data */
	rc = read(fd, crypt, size);
	if (rc != size)
		return -errno;

	/* check the data */
	if (size != compute_size(crypt->blocksize)) {
		free(crypt);
		return -EINVAL;
	}

	/* return the result */
	*result = crypt;
	return 0;
}

int crypt_save(struct crypt *crypt, int fd)
{
	size_t size;
	ssize_t rc;

	/* write the size */
	size = compute_size(crypt->blocksize);
	rc = write(fd, &size, sizeof size);
	if (rc != sizeof size)
		return -errno;

	/* write the data */
	rc = write(fd, crypt, size);
	if (rc != size)
		return -errno;

	return 0;
}

void crypt_destroy(struct crypt *crypt)
{
	free(crypt);
}

size_t crypt_encrypt(struct crypt *crypt, void *data, size_t size, int meld)
{
	uint8_t *d = data;
	size_t count = 0;
	uint16_t n = crypt->blocksize;

	for (count = 0 ; count + n <= size ; count += n) {
		if (meld && count)
			xor(&d[count], &d[count - n], n);
		shuffle(&d[count], crypt, 1);
	}

	for (count = 0 ; count + n <= size ; count += n)
		scramble(&d[count], crypt);

 	for (count = 0 ; count + n <= size ; count += n)
		docrypt(&d[count], crypt, 1);

	return count;
}

size_t crypt_decrypt(struct crypt *crypt, void *data, size_t size, int meld)
{
	uint8_t *d = data;
	size_t count = 0;
	uint16_t n = crypt->blocksize;

	for (count = 0 ; count + n <= size ; count += n)
		docrypt(&d[count], crypt, 0);

	for (count = 0 ; count + n <= size ; count += n)
		scramble(&d[count], crypt);

	for (count = 0 ; count + n <= size ; count += n) {
		shuffle(&d[count], crypt, 0);
		if (meld && count)
			xor(&d[count], &d[count - n], n);
	}

	return count;
}
