This code implement various aspects of OAuth2/OIDC items.

It provides implementation:
 - a resource client
 - a resource server
 - an authorization server
 - a token server
 - a link to cynara services
 - a AGL resource client service
 - a AGL resource server service

rfc7591
 2. forbid client_secret_post