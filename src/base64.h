/*
 * Copyright (C) 2017 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

enum base64_variant
{
	Base64_Variant_Standard = 0,
	Base64_Variant_Trunc = 1,
	Base64_Variant_URL = 2,
	Base64_Variant_URL_Trunc = 3
};

extern size_t base64_encode_length(
			size_t length,
			enum base64_variant variant);
extern void base64_encode_put(
			const char *buffer,
			size_t size,
			char **pout,
			int *psta,
			enum base64_variant variant);
extern void base64_encode_flush(
			char **pout,
			int *psta,
			enum base64_variant variant);

extern size_t base64_decode_length(size_t length);
extern void base64_decode_put(const char *buffer, size_t size, char **pout, int *psta);


extern char *base64_encode_array_variant(
			const char * const *args,
			size_t count,
			enum base64_variant variant);
extern char *base64_encode_multi_variant(
			const char * const *args,
			enum base64_variant variant);
extern char *base64_encode_variant(
			const char *arg,
			enum base64_variant variant);
extern char *base64_encode_buffer_variant(
			const void *buffer,
			size_t size,
			enum base64_variant variant);
extern char base64_encode_value_variant(
			int value,
			enum base64_variant variant);

#define base64_encode_array(args,count) \
		base64_encode_array_variant(args,count,Base64_Variant_Standard)
#define base64_encode_multi(args) \
		base64_encode_multi_variant(args,Base64_Variant_Standard)
#define base64_encode(arg) \
		base64_encode_variant(arg,Base64_Variant_Standard)
#define base64_encode_buffer(arg,sz) \
		base64_encode_buffer_variant(arg,sz,Base64_Variant_Standard)
#define base64_encode_value(val) \
		base64_encode_value_variant(val,Base64_Variant_Standard)

#define base64_encode_array_standard(args,count) \
		base64_encode_array_variant(args,count,Base64_Variant_Standard)
#define base64_encode_multi_standard(args) \
		base64_encode_multi_variant(args,Base64_Variant_Standard)
#define base64_encode_standard(arg) \
		base64_encode_variant(arg,Base64_Variant_Standard)
#define base64_encode_buffer_standard(arg,sz) \
		base64_encode_buffer_variant(arg,sz,Base64_Variant_Standard)
#define base64_encode_value_standard(val) \
		base64_encode_value_variant(val,Base64_Variant_Standard)

#define base64_encode_array_url(args,count) \
		base64_encode_array_variant(args,count,Base64_Variant_URL)
#define base64_encode_multi_url(args) \
		base64_encode_multi_variant(args,Base64_Variant_URL)
#define base64_encode_url(arg) \
		base64_encode_variant(arg,Base64_Variant_URL)
#define base64_encode_buffer_url(arg,sz) \
		base64_encode_buffer_variant(arg,sz,Base64_Variant_URL)
#define base64_encode_value_url(val) \
		base64_encode_value_variant(val,Base64_Variant_URL)

#define base64_encode_array_trunc(args,count) \
		base64_encode_array_variant(args,count,Base64_Variant_Trunc)
#define base64_encode_multi_trunc(args) \
		base64_encode_multi_variant(args,Base64_Variant_Trunc)
#define base64_encode_trunc(arg) \
		base64_encode_variant(arg,Base64_Variant_Trunc)
#define base64_encode_buffer_trunc(arg,sz) \
		base64_encode_buffer_variant(arg,sz,Base64_Variant_Trunc)

#define base64_encode_array_url_trunc(args,count) \
		base64_encode_array_variant(args,count,Base64_Variant_URL_Trunc)
#define base64_encode_multi_url_trunc(args) \
		base64_encode_multi_variant(args,Base64_Variant_URL_Trunc)
#define base64_encode_url_trunc(arg) \
		base64_encode_variant(arg,Base64_Variant_URL_Trunc)
#define base64_encode_buffer_url_trunc(arg,sz) \
		base64_encode_buffer_variant(arg,sz,Base64_Variant_URL_Trunc)



/* vim: set colorcolumn=80: */
