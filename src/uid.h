/*
 * Copyright (C) 2018 "IoT.bzh"
 * Author: José Bollo <jose.bollo@iot.bzh>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

struct uidset;
struct uid;

extern int uidset_create(struct uidset **result, int minlen);

extern struct uidset *uidset_addref(struct uidset *uidset);

extern void uidset_unref(struct uidset *uidset);

extern struct uid *uidset_search(struct uidset *uidset, const char *text);

extern unsigned uidset_count(struct uidset *uidset);

extern void uidset_for_all(
	struct uidset *uidset,
	void (*callback)(void *closure, struct uid *uid, void *data),
	void *closure
);

extern int uid_create(struct uid **result, struct uidset *uidset, void *data);

extern int uid_create_length(
	struct uid **result,
	struct uidset *uidset,
	void *data,
	int minlen
);

extern int uid_create_for_text(
	struct uid **result,
	struct uidset *uidset,
	const char *text,
	void *data
);

extern struct uid *uid_addref(struct uid *uid);

extern void uid_unref(struct uid *uid);

extern int uid_change(struct uid *uid);

extern const char *uid_text(struct uid *uid);

extern void *uid_data(struct uid *uid);

extern void uid_data_set(struct uid *uid, void *data);

extern struct uidset *uid_set(struct uid *uid);

