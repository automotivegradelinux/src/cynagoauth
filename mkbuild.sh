#/bin/sh

h="$(dirname $0)"

mkdir -p "$h/build" || exit
cd "$h/build" || exit

[ "$1" = "-f" ] && { rm -r * 2>/dev/null; shift; }
[ "$1" = "--force" ] && { rm -r * 2>/dev/null; shift; }

cmake \
	-DCMAKE_BUILD_TYPE=Debug \
	-DCMAKE_INSTALL_PREFIX=~/.local \
	..

make -j "$@"

