###########################################################################
# Copyright (C) 2015-2019 "IoT.bzh"
#
# author: José Bollo <jose.bollo@iot.bzh>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
###########################################################################

cmake_minimum_required(VERSION 3.3)

project(cynagoauth C)

set(PROJECT_DESCRIPTION "Simple authorization server using cynagora as backend for token check")
set(PROJECT_VERSION 0.1 CACHE STRING "Version of the project")

include(FindPkgConfig)
include(CheckIncludeFiles)
include(CheckLibraryExists)
include(GNUInstallDirs)
include(CTest)

add_subdirectory(src)

set(UNITDIR_SYSTEM "${CMAKE_INSTALL_FULL_LIBDIR}/systemd/system"
                  CACHE PATH "Path to systemd system unit files")
install(
  FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/cynagoauth.service
  DESTINATION
    ${UNITDIR_SYSTEM}
)
