SUMMARY
-------

This contains a basic OAuth2 authorization and token server:
cynagoauth-server.

It currently only implments the client credential flow, checking
the client identity using the Smack label. A tiny launcher,
cynagoauth-launch, is provided to negociate the token and run
the final client, setting CYNAGOAUTH_TOKEN environment
variable and substitute the patterns for the token of the
arguments of the launched program.

LICENSE
-------

This is released under the terms of APLv2 as explained
in file LICENSE.txt


DEPENDENCIES
------------

It depends of:

- json-c
- libmicrohttpd
- openssl
- libcurl
- cynagora

COMPILING
---------

To compile and install it:

```sh
mkdir build
cd build
cmake ..
make
```

RFCs
----

OAuth 2.0 Authorization Framework: <https://tools.ietf.org/html/rfc6749>

OAuth 2.0 Authorization Server Metadata: <https://tools.ietf.org/html/rfc8414>

OAuth 2.0 Dynamic Client Registration Protocol: <https://tools.ietf.org/html/rfc7591>

OpenID Connect Discovery 1.0: <https://openid.net/specs/openid-connect-discovery-1_0.html>

cynagoauth-server
-----------------

```text
> cynagoauth-server -h

usage: cynagoauth-server [options...] [interfaces...]

Run a basic OAuth server, currently only implementing client credential
flow based on Smack labels and Cynagora backend.

The interfaces specify ip adresses and port to listen. It must be of
the form [HOST][:SERVICE]. Default host: *, default port: 7777.
Examples:

  localhost:5555        listen on loopback on port 5555
  *:1234                listen any interface on port 1234
  localhost             listen on default port of localhost

Default interface if none is given: *:7777

Options:

 -h, --help        this help
 -s, --secure      serves https
 -u, --unsecure    serves http
```

cynagoauth-launcher
-------------------

```text
usage: cynagoauth-launch [options...] program [args...]

Ask an OAuth2 server for an access token and launches the given program
with this retrieved token. The URL of the token end point to be queried
can be set by option (see below) or environment variable CYNAGOAUTH_URL.
The default value is http://localhost:7777/tok

When launched the program has the following environment variables defined:

  - the access token   CYNAGOAUTH_TOKEN

The arguments of the program to launch are scanned and patterns for the token
are substituted by the effective value of the token. The default pattern is @t

Options:

 -h, --help             this help
 -n, --name NAME        name of the environement variable to set
 -r, --replace PATTERN  redefine the pattern to be replaced
 -t, --token TOKEN      the token to use, token end point is not queried
 -u, --url URL          URL of the token end point
```
